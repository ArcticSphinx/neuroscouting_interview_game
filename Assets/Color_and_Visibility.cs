﻿using UnityEngine;
using System.Collections;

public class Color_and_Visibility : MonoBehaviour {

	public int BALL_NUM; //int representing which ball this is (set in Unity)
	static int rNum; //create a static int to represent a random selection from all 6 balls 
	float onScreen = 0; 

	// Use this for initialization
	void Start () 
	{
		//don't render the object until needed
		renderer.enabled = false;
		rNum = Random.Range(1,6); //select a random number between 1 and 6

		if (BALL_NUM == rNum)
		{
			renderer.enabled = true; //render if ball's number matches rNum
		}

	}
	
	// Update is called once per frame
	void Update () 
	{

		float gameTime = Time.timeSinceLevelLoad; 	//set a variable equal to the amount of time since the level loaded
		                          	//will be used to determine time to stop rendering a ball

		//only set this if onScreen is <= gameTime
		if(onScreen <= gameTime) 
		{
			onScreen = gameTime; 
		}

		//this will only be true after
		if (gameTime >= onScreen)
		{
			renderer.enabled = false;
		}

		//after 20 seconds, make all balls disappear
		if (gameTime >= 20)
		{
			renderer.enabled = false;

		}
		//every 3 seconds, make a random ball appear - Ball exists for 2 seconds, then a 1 second delay before next ball
		else if((gameTime % 3) == 0 && gameTime != 2)
		{
			int rBall = Random.Range(1,6); //select a random number between 1 and 6

			if(rBall == rNum)
			{
				renderer.enabled = true;
				onScreen = gameTime + 2; //2 second delay before ball disappears
			}
		}
		//after 2 seconds, make target disappear, if the ball's number matches that of rNum
		else if(gameTime >= 2 && BALL_NUM == rNum) //put this in an 'else if' so it doesn't get called with preceeding 'if's
		{
			renderer.enabled = false;
		}

	}
}
